# Blackbox exporter

The blackbox exporter allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP, ICMP and gRPC.

## Running this software

```
$ docker build -t blackbox .
$ docker run blackbox
```

## Use with Prometheus

https://github.com/prometheus/blackbox_exporter
